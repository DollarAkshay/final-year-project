from PIL import Image
import zbarlight
import numpy as np
filepath = 'qr code.jpg'

class QR_CODE_SCANNER:
    def __init__(self,filepath):
        self.filepath = filepath
        
    def read_image(self):
        with open(self.filepath,'rb') as image_file:
            self.image = Image.open(self.filepath)

    def scan_and_generate_data(self):
        self.codes = zbarlight.scan_codes('qrcode',self.image)
        self.details = self.codes[0].decode('utf-8').split('>')[1][25:].strip('/')
            #print(details, "\n\n")

        self.data_list = []
        i = 0
        k = 0
        self.data = ""
            
        for j in range(0,len(self.details)-1):
            if self.details[j]=='"' and self.details[j+1]==" ":
                self.data_list.append(self.data)
                self.data = ""
            if self.details[j-1]=='"' and self.details[j]==" ":
                j+=1
            else:
                self.data+=self.details[j]

        self.dict_details = {}

        for i in range(0,len(self.data_list)):
            detail = self.data_list[i].split("=")
            for i in range(0,len(detail)):
                detail[i] = detail[i].strip('\"')
                self.dict_details[detail[0]] = detail[1]

        return self.dict_details
            #print(data_list)

scan1 = QR_CODE_SCANNER(filepath)
scan1.read_image()
data = scan1.scan_and_generate_data()
print(data)