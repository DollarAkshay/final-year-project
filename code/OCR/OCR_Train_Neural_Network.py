##############################################################
#                                                            #
#   This program                                             #
#                                                            #
#                                                            #
#   INPUT  : An image with list of well spaced characters    #
#   OUTPUT : Bounding Box around the characters              #
#   AUTHOR : Akshay, Devashish and Mohit                     #
#   DATE   : 9th April 2017                                  #
#                                                            #
##############################################################
import cv2
import datetime
import glob
import os
import PIL
import random
import traceback

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
#
#
#
def getData(dir, shuffle=False):

    data_files = []
    data_labels = []
    for i, ch in enumerate(char_set):
        ch_ascii = str(ord(ch))
        file_list =  sorted( glob.glob(dir+ch_ascii+ '\\*.png'))
        for file_name in file_list:
            data_files.append( file_name)
            data_labels.append( np.eye(output_features)[i] )

    if shuffle==True:
        combined_data = list(zip(data_files, data_labels))
        random.shuffle(combined_data)
        data_files, data_labels = zip(*combined_data)
        
    return data_files, data_labels
#
# 
def getImage(fileList):
    imageList = [np.expand_dims(cv2.imread(fileName, cv2.IMREAD_GRAYSCALE), 2) for fileName in fileList]
    return imageList
#
#
def conv2d(x, filter_weights, stride):
    # stride = [batch, height, width, channels]
    return tf.nn.conv2d(input=x, filter=filter_weights, strides=[1, stride, stride, 1], padding='SAME')
#
#
def maxpool2d(x, stride):
    # stride = [batch, height, width, channels]
    # ksize = [batch, height, width, channels]
    return tf.nn.max_pool(value=x, ksize=[1, stride, stride, 1], strides=[1, stride, stride, 1], padding='SAME')
#
#
def relu(x):
    return tf.nn.relu(x)
#
#
def neural_network_model():
    
    # Weights
    with tf.name_scope('Weights'):
        # Variables
        weights_conv1 = tf.Variable(tf.random_normal([7, 7, 1, 32]) , name="Weights_Conv1")
        weights_conv2 = tf.Variable(tf.random_normal([5, 5, 32, 48]), name="Weights_Conv2")
        weights_conv3 = tf.Variable(tf.random_normal([3, 3, 48, 64]), name="Weights_Conv3")
        weights_conv4 = tf.Variable(tf.random_normal([3, 3, 64, 96]), name="Weights_Conv4")
        weights_fc5 = tf.Variable(tf.random_normal([96, 1024]), name="Weights_FC5")
        weights_out = tf.Variable(tf.random_normal([1024, output_features]), name="Weights_Out")
        # Summaries
        tf.summary.histogram('Weights Conv1', weights_conv1)
        tf.summary.histogram('Weights Conv2', weights_conv2)
        tf.summary.histogram('Weights Conv3', weights_conv3)
        tf.summary.histogram('Weights Conv4', weights_conv4)
        tf.summary.histogram('Weights FC5', weights_fc5)
        tf.summary.histogram('Weights Out', weights_out)

    # Biases
    with tf.name_scope('Biases'):
        # Variables  
        biases_conv1 = tf.Variable(tf.random_normal([32]), name="Biases_Conv1")
        biases_conv2 = tf.Variable(tf.random_normal([48]), name="Biases_Conv2")
        biases_conv3 = tf.Variable(tf.random_normal([64]), name="Biases_Conv3")
        biases_conv4 = tf.Variable(tf.random_normal([96]), name="Biases_Conv4")
        biases_fc5 = tf.Variable(tf.random_normal([1024]), name="Biases_FC5")
        biases_out = tf.Variable(tf.random_normal([output_features]), name="Biases_Out")
        # Summaries
        tf.summary.histogram('Biases Conv1', biases_conv1)
        tf.summary.histogram('Biases Conv2', biases_conv2)
        tf.summary.histogram('Biases Conv3', biases_conv3)
        tf.summary.histogram('Biases Conv4', biases_conv4)
        tf.summary.histogram('Biases FC5', biases_fc5)
        tf.summary.histogram('Biases Out', biases_out)

    # Neural Network
    tf.summary.image('1_Input_Images', input_placeholder)
    with tf.name_scope('Convolution_Layer_1'):
        conv1 = tf.add(conv2d(input_placeholder, weights_conv1, 1), biases_conv1)
        tf.summary.image('Input_Images Conv1', tf.reshape(conv1 , shape=[-1, 64*8, 64*4, 1]))
        relu1 = relu(conv1)
    with tf.name_scope('Pooling_Layer_1'):
        pool1 = maxpool2d(relu1, 4)
        tf.summary.image('Input_Images Pool1', tf.reshape(pool1 , shape=[-1, 16*8, 16*4, 1]))

    with tf.name_scope('Convolution_Layer_2'):
        conv2 = tf.add(conv2d(pool1, weights_conv2, 1), biases_conv2)
        tf.summary.image('Input_Images Conv2', tf.reshape(conv2 , shape=[-1, 16*8, 16*6, 1]))
        relu2 = relu(conv2)
    with tf.name_scope('Pooling_Layer_2'):
        pool2 = maxpool2d(relu2, 2)
        tf.summary.image('Input_Images Pool2', tf.reshape(pool2 , shape=[-1, 8*8, 8*6, 1]))

    with tf.name_scope('Convolution_Layer_3'):
        conv3 = tf.add(conv2d(pool2, weights_conv3, 1), biases_conv3)
        tf.summary.image('Input_Images Conv3', tf.reshape(conv3 , shape=[-1, 8*8, 8*8, 1]))
        relu3 = relu(conv3)
    with tf.name_scope('Pooling_Layer_3'):
        pool3 = maxpool2d(relu3, 2)
        tf.summary.image('Input_Images Pool3', tf.reshape(pool3 , shape=[-1, 4*8, 4*8, 1]))

    with tf.name_scope('Convolution_Layer_4'):
        conv4 = tf.add(conv2d(pool3, weights_conv4, 1), biases_conv4)
        tf.summary.image('Input_Images Conv4', tf.reshape(conv4 , shape=[-1, 4*12, 4*8, 1]))
        relu4 = relu(conv4)
    with tf.name_scope('Pooling_Layer_4'):
        pool4 = maxpool2d(relu4, 4)
        tf.summary.image('Input_Images Pool4', tf.reshape(pool4 , shape=[-1, 1*12, 1*8, 1]))
        
    with tf.name_scope('Fully_Connected_Layer_5'):
        pool4 = tf.reshape(pool4 , shape=[-1, 96])
        fc5 = tf.add(tf.matmul(pool4,weights_fc5), biases_fc5)

    with tf.name_scope('Output_Layer'):
        output = tf.add(tf.matmul(fc5, weights_out), biases_out)
        output = tf.reshape(output , shape=[-1, output_features])
        return output
#
#
def train_model():
    global global_step
    
    saver = tf.train.Saver()
    merged_summary = tf.summary.merge_all()
    with tf.Session() as sess :
        if os.path.isfile(os.path.join(LOG_DIR, 'latest-model.ckpt.index')):
            saver.restore(sess, os.path.join(LOG_DIR, 'latest-model.ckpt'))
            print("\nTrained Model restored")
        else:
            sess.run(tf.global_variables_initializer())
            print("\nNew Model Initialized")
        summary_writer = tf.summary.FileWriter(os.path.join(LOG_DIR, 'TensorBoard/'+datetime.datetime.now().strftime("%b %d,  %I-%M-%S %p")))
        summary_writer.add_graph(sess.graph)
        
        # Train Neural Net
        print("\nTraining Neural Net ....")
        try :
            ephocs = 10
            for epoch in range(ephocs):
                epoch_loss = 0
                for i in range(0, len(train_files), BATCH_SIZE):
                    train_images = getImage( train_files[i:i+BATCH_SIZE] ) 
                    _, l = sess.run([optimizer, loss], feed_dict={input_placeholder: train_images, output_placeholder: train_labels[i:i+BATCH_SIZE]} )
                    epoch_loss += l
                print("Epoch", epoch+1,"completed with a cost of", epoch_loss)
                # Write summary every epoch
                if epoch%10==0:
                    save_path = saver.save(sess, os.path.join(LOG_DIR, 'TensorBoard/model.ckpt'))
                if epoch%1==0:
                    for i in range(0, len(train_files), BATCH_SIZE):
                        train_images = getImage( train_files[i:i+BATCH_SIZE] ) 
                        batch_summary= sess.run(merged_summary, feed_dict={input_placeholder: train_images, output_placeholder: train_labels[i:i+BATCH_SIZE]} )
                        summary_writer.add_summary(batch_summary, global_step)
                        global_step+=1
                        summary_writer.flush()
                        epoch_loss += l

        except KeyboardInterrupt:
            print("\nKeyboard Interrupt")
        except Exception as e:
            print("\nUnknown Exception")
            print(traceback.format_exc())
            print(str(e))
        finally:
            save_path = saver.save(sess, os.path.join(LOG_DIR, 'latest-model.ckpt'))
            print("Saving Model in %s ...." % save_path)
            summary_writer.close()
#
#
def predict_model():

    saver = tf.train.Saver()
    with tf.Session() as sess :
        saver.restore(sess, os.path.join(LOG_DIR, 'latest-model.ckpt'))
        print("Trained Model restored")
        
        # Predict Test Data
        try :
            print("\nPredicting Test Data ....")
            # Get Predicted Test labels
            labels = []
            acc_array = []
            for i in range(0, len(test_files), BATCH_SIZE):
                test_images = getImage( test_files[i:i+BATCH_SIZE] )
                batch_labels, batch_acc = sess.run([test_prediction, accuracy], feed_dict={input_placeholder: test_images, output_placeholder: test_labels[i:i+BATCH_SIZE]} )
                batch_labels = test_prediction.eval(feed_dict={input_placeholder : test_images})
                labels.extend(batch_labels)
                acc_array.append(100*batch_acc*len(test_images))

            # Check Accuracy
            correct = 0
            char_correct = 0
            for i, label in enumerate(labels):
                expected_label = np.argmax(test_labels[i])
                if  expected_label != label:
                    if char_set[expected_label].upper() == char_set[label].upper():
                        char_correct+=1
                    print(""+char_set[expected_label]+"/"+char_set[label]+"   File : "+ test_files[i])
                else:
                    correct+=1
            print("\n\nAccuracy :", 100*correct/len(labels))
            print("Case Insensitive Accuracy :", 100*(correct+char_correct)/len(labels))
            print("\n")
        except KeyboardInterrupt:
            print("\nKeyboard Interrupt")
        except Exception as e:
            print("\nUnknown Exception")
            print(traceback.format_exc())
            print(str(e))
#
#
#
drivers_license_set = [
    '0','1','2','3','4','5','6','7','8','9',
    'A','B','C','D','E','F','G','H','I','J',
    'K','L','M','N','O','P','Q','R','S','T',
    'U','V','W','X','Y','Z',
    '#','?','&','@','%','\\','/'
]

alphabet_set = [
    'A','B','C','D','E','F','G','H','I','J',
    'K','L','M','N','O','P','Q','R','S','T',
    'U','V','W','X','Y','Z',
    'a','b','c','d','e','f','g','h','i','j',
    'k','l','m','n','o','p','q','r','s','t',
    'u','v','w','x','y','z'
]

uppercase_alphabet_set = [
    'A','B','C','D','E','F','G','H','I','J',
    'K','L','M','N','O','P','Q','R','S','T',
    'U','V','W','X','Y','Z'
]

digit_set = [
    '0','1','2','3','4','5','6','7','8','9',
]

char_set = uppercase_alphabet_set

global_step = 0
if char_set == drivers_license_set:
    LOG_DIR = "code/OCR/Tensorflow Logs/Drivers License/"
elif char_set == alphabet_set:
    LOG_DIR = "code/OCR/Tensorflow Logs/Alphabets/"
elif char_set == uppercase_alphabet_set:
    LOG_DIR = "code/OCR/Tensorflow Logs/Uppercase Alphabets/"
elif char_set == digit_set:
    LOG_DIR = "code/OCR/Tensorflow Logs/Digits/"
else:
    LOG_DIR = "code/OCR/Tensorflow Logs/Unknown"

TRAIN_DIR = "code/OCR/Training Data/"
TEST_DIR = "code/OCR/Testing Data/"
SIZE = 64
BATCH_SIZE = 128
input_features = [None, SIZE, SIZE, 1]
output_features = len(char_set)
with tf.name_scope('Inputs'):
    input_placeholder = tf.placeholder('float', input_features, name='image_input')
    output_placeholder = tf.placeholder('float', [None, output_features], name='label_input')

#Train Data
print("\nLoading Training Data ....", end="")
train_files,train_labels = getData(TRAIN_DIR, True)
print(len(train_files),"\n")

#Test Data
print("\nLoading Testing Data ....", end="")
test_files,test_labels = getData(TEST_DIR)
print(len(test_files),"\n")



output_prediction = neural_network_model()

with tf.name_scope('Loss'):
    with tf.name_scope('Cross_Entropy'):
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=output_prediction, labels=output_placeholder)
    loss = tf.reduce_mean( cross_entropy)
    tf.summary.scalar("Loss Graph", loss)
with tf.name_scope('Trainer'):
    trainer = tf.train.AdamOptimizer()
with tf.name_scope('Optimizer'):
    optimizer = trainer.minimize(loss)
with tf.name_scope('Prediction'):
    test_prediction = tf.argmax(output_prediction, 1)
with tf.name_scope('Accuracy'):
    correct_prediction = tf.equal(tf.argmax(output_prediction, 1), tf.argmax(output_placeholder, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    tf.summary.scalar("Accuracy Graph", accuracy)

train_model()

predict_model()

