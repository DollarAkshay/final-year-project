##############################################################
#                                                            #
#   This program will get the details on the Driver's        #
#   License from an Image                                    #
#                                                            #
#   INPUT  : An image of a Driver's License                  #
#   OUTPUT : Dictionary with Driver's License Details        #
#   AUTHOR : Akshay, Devashish and Mohit                     #
#   DATE   : 7th May 2017                                    #
#                                                            #
##############################################################

import os
import cv2
import numpy as np

import OCR_Prediction as OCR

def rotateImage(image, angle, center = None):
    (h, w) = image.shape[:2]
    if center is None:
        center = (w / 2, h / 2)
    M = cv2.getRotationMatrix2D(center, angle, 1)
    rotated = cv2.warpAffine(image, M, (w, h))
    return rotated
#
#
def sortContoursByX(contours):
    boundingBoxes = [cv2.boundingRect(cnt) for cnt in contours]
    groupedArray =  zip(contours, boundingBoxes)
    sortedGroupedArray = sorted(groupedArray, key=lambda obj : obj[1][0])
    (contours, boundingBoxes) = zip(*sortedGroupedArray)
    return contours
# 
# 
def findLargestContour(image):
    img_cont, contours, hierarchy = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    card_contour = None
    max_cont_size = 0
    for cnt in contours:
        x,y,w,h = cv2.boundingRect(cnt)
        if w*h > max_cont_size:
            card_contour = cnt
            max_cont_size = w*h
    return card_contour
#
#
def rotateCard(image):
    #Convert Image Colour
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    # Get Card Threshold
    ret, thresh = cv2.threshold(image_gray, 100, 255, cv2.THRESH_BINARY)
    kernel = np.ones((10, 10),np.uint8)
    morph = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
    kernel = np.ones((50, 50),np.uint8)
    morph = cv2.morphologyEx(morph, cv2.MORPH_CLOSE, kernel)

    # Find Min Rect Contour
    card_contour = findLargestContour(morph)
    rect = cv2.minAreaRect(card_contour)
    box = cv2.boxPoints(rect)
    box = np.int0(box)

    # Rotate Image
    x_center = int(np.mean([p[0] for p in box]))
    y_center = int(np.mean([p[1] for p in box]))
    center = ( x_center, y_center)
    # cv2.circle(image, center, 20, (30, 200, 30), 20)
    angle = rect[2]
    if angle < -45:
        angle += 90
    rotated = rotateImage(image, angle, center)

    return rotated
#
#
def squareResizeandPad(image, size, paddingFraction):
    (h, w) = image.shape[:2]

    padValue = int(paddingFraction * max(h, w) / 2)
    pad = [ padValue, padValue, padValue, padValue]
    if h >= w:
        extra = h - w
        pad[2] += (extra+1)//2
        pad[3] += extra//2
    else:
        extra = w - h
        pad[0] += (extra+1)//2
        pad[1] += extra//2

    padded_image = cv2.copyMakeBorder(image, pad[0], pad[1], pad[2], pad[3], cv2.BORDER_CONSTANT, value=[0, 0, 0])
    resized_image = cv2.resize(padded_image, size, interpolation = cv2.INTER_AREA)
    return resized_image
#
#
def extractCharacters(image):
    res = []
    img_cont, contours, hierarchy = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = sortContoursByX(contours)
    for cnt in contours:
        x,y,w,h = cv2.boundingRect(cnt)
        coppped_char = image[y:y+h, x:x+w]
        resized_char = squareResizeandPad(coppped_char, (64, 64), 0.8)
        res.append(resized_char)
    return res
#
#
def cropCard(image):
    #Convert Image Colour
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    # Get Card Threshold
    ret, thresh = cv2.threshold(image_gray, 100, 255, cv2.THRESH_BINARY)
    kernel = np.ones((10, 10),np.uint8)
    morph = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
    kernel = np.ones((50, 50),np.uint8)
    morph = cv2.morphologyEx(morph, cv2.MORPH_CLOSE, kernel)

    # Find Countour and Crop
    card_contour = findLargestContour(morph)
    x,y,w,h = cv2.boundingRect(card_contour)
    coppped_image = image[y:y+h, x:x+w]

    return coppped_image
#
#
view_scale = 1/5
img = cv2.imread('code/ID Cards/Drivers Licence 1 rot.jpg')
# cv2.imshow('Original', cv2.resize(img, None, fx = view_scale, fy = view_scale))

img = rotateCard(img)
# cv2.imshow('Original Rotated', cv2.resize(img, None, fx = view_scale, fy = view_scale))

img = cropCard(img)
(h, w) = img.shape[:2]
print("Size of Cropped Image = ", h, "x", w)
# cv2.imshow('Original Crop', cv2.resize(img, None, fx = view_scale, fy = view_scale))


# Convert Image to Threshold
img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
thresh = cv2.adaptiveThreshold(img_gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 21, 5)

# Apply Morphological Transforms
kernel = np.ones((5,5),np.uint8)
morph = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
kernel = np.ones((6, 6),np.uint8)
morph = cv2.erode(morph, kernel, iterations = 1)
kernel = np.ones((3, 3),np.uint8)
morph = cv2.dilate(morph, kernel, iterations = 2)

# cv2.imshow('Threshold', cv2.resize(thresh, None, fx = view_scale, fy = view_scale))
# cv2.imshow('Morph', cv2.resize(morph, None, fx = view_scale, fy = view_scale))
char_dict = {}

id_mask = cv2.imread('code/ID Cards/Drivers Licence ID Mask.png', cv2.IMREAD_GRAYSCALE )
id_mask = cv2.resize(id_mask, (w, h), interpolation = cv2.INTER_NEAREST)
id_img = cv2.bitwise_and(morph ,id_mask)
kernel = np.ones((5,5),np.uint8)
id_img = cv2.morphologyEx(id_img, cv2.MORPH_OPEN, kernel)
char_dict['id'] = extractCharacters(id_img)


name_mask = cv2.imread('code/ID Cards/Drivers Licence Name Mask.png', cv2.IMREAD_GRAYSCALE )
name_mask = cv2.resize(name_mask, (w, h), interpolation = cv2.INTER_NEAREST)
name_img = cv2.bitwise_and(morph, name_mask)
kernel = np.ones((5,5),np.uint8)
name_img = cv2.morphologyEx(name_img, cv2.MORPH_OPEN, kernel)
char_dict['name'] = extractCharacters(name_img)

dob_mask = cv2.imread('code/ID Cards/Drivers Licence DoB Mask.png', cv2.IMREAD_GRAYSCALE )
dob_mask = cv2.resize(dob_mask, (w, h), interpolation = cv2.INTER_NEAREST)
dob_img = cv2.bitwise_and(morph, dob_mask)
kernel = np.ones((5,5),np.uint8)
dob_img = cv2.morphologyEx(dob_img, cv2.MORPH_OPEN, kernel)
char_dict['dob'] = extractCharacters(dob_img)

digits = []
letters = []

OCR.initGlobals(3)

result = []
k = 0
for key in char_dict:
    for i, char_img in enumerate(char_dict[key]):
        cv2.imwrite("code/OCR/Extracted Images/"+str(k)+".png", char_img)
        k+=1
cv2.waitKey(0)