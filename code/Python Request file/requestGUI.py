import tkinter as tk
import json
import requests
from datetime import datetime

def make_request():  
    #get the data from text field
    identityNumber = E_IdentityNumber.getint()
    Name = E_firstName.get()
    lastName = E_lastName.get()
    guardianName = E_fatherName.get()
    state = E_state.get()
    
    #print(datetime.now().strftime('%Y/%m/%d %H:%M:%S'))
    
    #local host URL
    URL = 'http://192.168.0.104:8000/home/addData/'
    
    #heder of the request
    header = {'content-type':'application/json'}
    
    #data to be send
    new_data = {'identity_no':identityNumber,\
    'name':firstName,\
    'gender':lastName,'guardn_name':fatherName,\
    'Date':datetime.now().strftime('%Y-%m-%d %H:%M:%S')\
    ,'state':state}

    response = requests.post(URL,headers=header,json=new_data)
    
    if(response.status_code==200):
        print("Data Entered Successfully")

    if(response.text=="Invalid"):
        print(response.text)


project = tk.Tk()


#Identity Number
L_IdentityNumber = tk.Label(project,text = "Identity Number")
L_IdentityNumber.pack()
E_IdentityNumber = tk.Entry(project,bd=5)
E_IdentityNumber.pack()

#firstName
L_firstName = tk.Label(project,text = "First Name")
L_firstName.pack()
E_firstName = tk.Entry(project,bd=5)
E_firstName.pack()

#LastName
L_lastName = tk.Label(project,text = "Last Name")
L_lastName.pack()
E_lastName = tk.Entry(project,bd=5)
E_lastName.pack()


#fatherName
L_fatherName = tk.Label(project,text = "Father's Name")
L_fatherName.pack()
E_fatherName = tk.Entry(project,bd=5)
E_fatherName.pack()


#state
L_state = tk.Label(project,text = "State")
L_state.pack()
E_state = tk.Entry(project,bd=5)
E_state.pack()



b = tk.Button(project,text="Send Data",command=make_request)
b.pack()



project.mainloop()
