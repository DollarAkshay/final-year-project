from django.db import models

# Create your models here.

class Visitors(models.Model):
    identity_no = models.IntegerField()
    name = models.CharField(max_length=25)
    guardian_name = models.CharField(max_length=25)
    gender = models.CharField(max_length=1)
    visiting_date = models.DateTimeField()
    year_of_birth = models.IntegerField()
    state = models.CharField(max_length=25)
    
    def __str__(self):
        return self.name
