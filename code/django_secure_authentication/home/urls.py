from django.conf.urls import url
from django.views.generic import ListView
from home.models import Visitors
from django.db.models import Count
from . import views
urlpatterns = [
    url(r'^$',views.index),
    #to display weekly data
    url(r'^/week',ListView.as_view(queryset=Visitors.objects.raw("Select date(visiting_date) as id, count(date(visiting_date)) \
    as count_date from home_visitors \
    where date(visiting_date)>date('now','-7 day') group by date(visiting_date)")
    ,template_name="home/week.html")),
    
    #to display number of visitors from each state
    url(r'^/state',ListView.as_view(queryset=Visitors.objects.raw("Select LOWER(state) as id, count(LOWER(state)) \
    as count_data from home_visitors \
    group by LOWER(state)")
    ,template_name="home/state.html")),

    #to add data
    url(r'addData',views.addData)
]
