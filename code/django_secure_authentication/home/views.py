from django.shortcuts import render
from django.http import HttpResponseRedirect
from home.models import Visitors
from django.views.decorators.csrf import csrf_exempt
from django.middleware.csrf import get_token
import json

# Create your views here.
def index(request):
    return render(request,'home/homepage.html')

@csrf_exempt
def addData(request):
    if(request.method=='POST'):
        data = request.body.decode("utf-8")  #returns data in string 
        qd = json.loads(data) #converts to dict
        print(type(qd))
        
        identity_no = qd["uid"]
        print(identity_no)
        name = qd['name']
        guardian_name = qd['guardn_name']
        date = qd['date']
        gender = qd['gender']
        state = qd['state']
        yob = qd['yob']

        if state=="" or date== None or gender=="" or identity_no==None or name=="" or guardian_name=="":
            return HttpResponse("Invalid")
        else:       
            visitor_obj = Visitors(identity_no=identity_no,
            name = name,
            guardian_name = guardian_name, gender = gender,state=state,visiting_date = date,
            year_of_birth = yob)
            visitor_obj.save()
        
        return HttpResponseRedirect('/home')
        