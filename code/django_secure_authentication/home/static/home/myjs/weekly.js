
//For MainHeader Chart
//print(labels[0]);

var data = {
  // A labels array that can contain any sort of values
  labels: labels,
  // Our series array that contains series objects or in this case series data arrays
  series:[series]
};

var options={
  width:window.width,
  height:'400'
};

//var options = {width:window.width,height:200};
// Create a new line chart object where as first parameter we pass in a selector
// that is resolving to our chart container element. The Second parameter
// is the actual data object.
new Chartist.Line('.ct-chart', data,options);
//For Pie chart

